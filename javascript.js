
//calc button
var btn = document.getElementById('calculate');

//onclick
btn.onclick = function calculate() {
  //save the noTax value
  var noTax = document.getElementById('value').value;
  //calculate tax only
  var tax = (parseFloat(noTax) * 0.21).toFixed(2);
  //calculate the withTax
  var withTax = (parseFloat(noTax) * 1.21).toFixed(2);

  //Write the noTax
  document.getElementById('noTax').innerHTML = "No Tax: " + noTax;
  //Write the tax only
  document.getElementById('onlyTax').innerHTML = "Only Tax: " + tax;
  //Write the withTax
  document.getElementById('withTax').innerHTML = "Incl Tax: " + withTax;
}
